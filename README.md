Dotoo - Basic todo management system

Tasks are organized into separate todo_*.txt files, that can be
scattered around the file system, or even across other machines.  These
tools provide ways to summarize, search, update, and manipulate the
lists.

