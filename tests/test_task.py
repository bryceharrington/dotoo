#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os.path
import pytest

#sys.path.insert(0, os.path.realpath(os.path.join(os.path.dirname(__file__), "..")))

from dotoo.task import Task

class TestTask(object):
    def test_object(self):
        task = Task("testtask")
        assert(task)

    def test_description(self):
        desc = "test-description"
        task = Task(desc)
        assert(task.description == desc)
