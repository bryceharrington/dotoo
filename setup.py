#!/usr/bin/env python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
#
# Copyright (c) 2019 - Bryce W. Harrington

from setuptools import setup, find_packages

import re
import glob

def get_version(package):
    """Directly retrieve version, avoiding an import

    Since setup.py runs before the package is set up, we can't expect
    that simply doing an import ._version will work reliably in all
    cases.  Instead, manually import the version from the file here,
    and then the module can be imported elsewhere in the project easily.
    """
    version_file = "%s/%s" %(package, '_version.py')
    version_string = open(version_file, "rt").read()
    re_version = r"^__version__ = ['\"]([^'\"]*)['\"]"
    m = re.search(re_version, version_string, re.M)
    if not m:
        raise RuntimeError("Unable to find version string for %s in %s." %(
            package, version_file))
    return m.group(1)

def get_description():
    return open('README.md', 'rt').read()

setup(
    name             = 'dotoo',
    version          = get_version('dotoo'),
    url              = 'none',
    author           = 'Bryce Harrington',
    author_email     = 'bryce@bryceharrington.org',
    description      = 'Todo list management toolkit',
    long_description = get_description(),
    keywords         = ['task', 'todo', 'gtd', 'planning'],
    classifiers      = [
        # See https://pypi.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Information Technology',
        'Intended Audience :: System Administrators',
        'Topic :: Software Development :: Build Tools',
        'Topic :: Software Development :: Testing',
        'Topic :: System :: Operating System',
        'Topic :: System :: System Shells',
        'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',
        'Programming Language :: Python :: 3',
        ],
    platforms        = ['any'],
    python_requires  = '>=3',
    setup_requires   = [
        'pytest-runner'
        ],
    tests_require    = [
        'pytest'
        'pep8',
        'pyflakes',
        ],
    install_requires = [
        'argparse',
        'yaml',
        'fnmatch',
        'requests',
        ],
    setup_requires   = ['pytest-runner'],
    packages         = find_packages(),
    package_data     = { },
    data_files       = [ ],
    scripts          = [
        'scripts/dotoo',
        'scripts/daily',
        'scripts/meals',
        'scripts/std',
        ],

    tests_require    = ['pytest'],
)
